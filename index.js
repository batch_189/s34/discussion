// EXPRESS.JS
/*
    Opinionated Web Framework
        - speeds, enforces, lack of flexibility,

    UnOpinionated Web Framework
        - dev dictates how to use the framework
        - flexibility, no "right way" of structuring an application
        - abundance of options may be overwhelming.

    Advantages
        - Makes it easy to learn and use
        - offers ready to use components for the most common wed dev needs.
        - Adds easier routing and processing of HTTP methods
*/

// EXPRESS SETUP
/*
    1. Import express by using the 'require' directive
    2. Use the express() function and assign it to the app variable
    3. Declare a var for the port of this server
    4. Use these middleware to enable our server to read JSON as regular JS
    5. you can then have your route after all of that.
*/
const { request, response } = require('express');
const express = require ('express');
const app = express();

const port = 3000

// allowing server to read json
app.use(express.json());
// allowing the server to read string
app.use(express.urlencoded({extended: true}));

// ROUTES
// Landing page route
app.get('/', (req, res) => {
    res.send('Hello!'); 
});

// Post request route
app.post('/hello', (req, res) => {
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

let users = [];

// Register user route
app.post('/register', (request, response) => {
    console.log(request.body)

    if (request.body.username !== '' && request.body.password !== ''){
        users.push(request.body)
        console.log(users)
        response.send(`User ${request.body.username} successfully registered!`)
    } else {
        response.send('Please input BOTH username ans password')
    }
});


let message
app.put('/change-password', (request, response) => {
/*
    Loop through the whole users array (declared above) so that the server 
    can check if the username that was inputted from the request matches any of 
    the existing usernames inside that array.
*/
    for (let i = 0; i < users.length; i++){
            console.log(users)
        if (request.body.username == users[i].username){ 
// If a username matches, re-assign/change the existing user's password.
            users[i].password = request.body.password

            message = `User ${request.body.username}'s password has been updated!`

            break
        } else {
            message = 'User does not exist.'
        }
    };
    response.send(message);
});

app.listen(port, () => console.log(`Server is now running at localhost: ${port}`));
